﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaOne
{
    public class Size
    {
        private string size;
        private double price;

        public Size(string size, double price)
        {
            this.size = size;
            this.price = price;
         
        }

        public double Price
        {
            get { return price; }
        }

        public override string ToString()
        {
            return size;
        }
    }


}
