﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolMarks;

namespace UnitTestProject1
{
    [TestClass]
    public class MarksBiggest
    {
        [TestMethod]
        public void GethighTest()
        {
            Marks marks = new Marks();

            marks.Add(5.5);
            marks.Add(4.0);
            marks.Add(5.5);
            marks.Add(5.5);
            double highExpected = 5.5;
            Assert.AreEqual(expected: highExpected, marks.Biggest());


        }



        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTestFull()
        {
            Marks marks = new Marks();

           
            double highExpected = 0;
            
            Assert.AreEqual(expected: highExpected, marks.Biggest());


        }

    }
}
