﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    public class Student
    {
        private string firstname;
        private string lastname;
        private string schoolClass;

        public Student(string firstname, string lastname)
        {
            if (!Char.IsUpper(firstname[0])) throw new Exception("Bad firstname");

            this.firstname = firstname;
            this.lastname = lastname.ToUpper();
        }

        public string Firstname
        {
            get { return firstname; }
        }

        public string Lastname
        {
            get { return lastname; }
        }

        public string SchoolClass
        {
            get
            {
                return schoolClass;
            }
            set
            {
                string way = value.Substring(3, 2);
                string yearAndSibling = value.Substring(value.Length - 2);

                if (!value.StartsWith("SI-")) throw new Exception("Bad class name");
                switch (value.Length)
                {
                    case 6:
                        if (!(way[0] == 'C')) throw new Exception("Bad class name");
                        break;
                    case 7:
                        if (!(way == "MI" || way == "CA")) throw new Exception("Bad class name");
                        break;
                    default: throw new Exception("Bad class name");
                }
                if (!(yearAndSibling[0] >= '1' && yearAndSibling[0] <= '4')) throw new Exception("Bad class name");
                if (!(yearAndSibling[1] >= 'a' && yearAndSibling[1] <= 'z')) throw new Exception("Bad class name");

                schoolClass = value;
            }
        }

        public override string ToString()
        {
            return $"{lastname} {firstname} ({schoolClass})";
        }
    }
}
