﻿namespace ClientRegister
{
    partial class PizzaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddPizza = new System.Windows.Forms.Button();
            this.checkBoxJam = new System.Windows.Forms.CheckBox();
            this.checkBoxMushrooms = new System.Windows.Forms.CheckBox();
            this.checkBoxVegi = new System.Windows.Forms.CheckBox();
            this.radioButtonSmall = new System.Windows.Forms.RadioButton();
            this.radioButtonMid = new System.Windows.Forms.RadioButton();
            this.radioButtonBig = new System.Windows.Forms.RadioButton();
            this.groupeBoxeSize = new System.Windows.Forms.GroupBox();
            this.groupeBoxIngredients = new System.Windows.Forms.GroupBox();
            this.groupeBoxeSize.SuspendLayout();
            this.groupeBoxIngredients.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddPizza
            // 
            this.AddPizza.Location = new System.Drawing.Point(91, 280);
            this.AddPizza.Name = "AddPizza";
            this.AddPizza.Size = new System.Drawing.Size(154, 45);
            this.AddPizza.TabIndex = 0;
            this.AddPizza.Text = "Valider";
            this.AddPizza.UseVisualStyleBackColor = true;
            this.AddPizza.Click += new System.EventHandler(this.AddPizza_Click);
            // 
            // checkBoxJam
            // 
            this.checkBoxJam.AutoSize = true;
            this.checkBoxJam.Location = new System.Drawing.Point(41, 46);
            this.checkBoxJam.Name = "checkBoxJam";
            this.checkBoxJam.Size = new System.Drawing.Size(63, 17);
            this.checkBoxJam.TabIndex = 1;
            this.checkBoxJam.Text = "Jambon";
            this.checkBoxJam.UseVisualStyleBackColor = true;
            // 
            // checkBoxMushrooms
            // 
            this.checkBoxMushrooms.AutoCheck = false;
            this.checkBoxMushrooms.AutoSize = true;
            this.checkBoxMushrooms.Location = new System.Drawing.Point(43, 85);
            this.checkBoxMushrooms.Name = "checkBoxMushrooms";
            this.checkBoxMushrooms.Size = new System.Drawing.Size(61, 17);
            this.checkBoxMushrooms.TabIndex = 2;
            this.checkBoxMushrooms.Text = "Champi";
            this.checkBoxMushrooms.UseVisualStyleBackColor = true;
            // 
            // checkBoxVegi
            // 
            this.checkBoxVegi.AutoSize = true;
            this.checkBoxVegi.Location = new System.Drawing.Point(43, 128);
            this.checkBoxVegi.Name = "checkBoxVegi";
            this.checkBoxVegi.Size = new System.Drawing.Size(47, 17);
            this.checkBoxVegi.TabIndex = 3;
            this.checkBoxVegi.Text = "Vegi";
            this.checkBoxVegi.UseVisualStyleBackColor = true;
            // 
            // radioButtonSmall
            // 
            this.radioButtonSmall.AutoSize = true;
            this.radioButtonSmall.Location = new System.Drawing.Point(23, 46);
            this.radioButtonSmall.Name = "radioButtonSmall";
            this.radioButtonSmall.Size = new System.Drawing.Size(52, 17);
            this.radioButtonSmall.TabIndex = 5;
            this.radioButtonSmall.TabStop = true;
            this.radioButtonSmall.Text = "Petite";
            this.radioButtonSmall.UseVisualStyleBackColor = true;
            // 
            // radioButtonMid
            // 
            this.radioButtonMid.AutoSize = true;
            this.radioButtonMid.Location = new System.Drawing.Point(23, 85);
            this.radioButtonMid.Name = "radioButtonMid";
            this.radioButtonMid.Size = new System.Drawing.Size(69, 17);
            this.radioButtonMid.TabIndex = 6;
            this.radioButtonMid.TabStop = true;
            this.radioButtonMid.Text = "Moyenne";
            this.radioButtonMid.UseVisualStyleBackColor = true;
            // 
            // radioButtonBig
            // 
            this.radioButtonBig.AutoSize = true;
            this.radioButtonBig.Location = new System.Drawing.Point(23, 128);
            this.radioButtonBig.Name = "radioButtonBig";
            this.radioButtonBig.Size = new System.Drawing.Size(60, 17);
            this.radioButtonBig.TabIndex = 7;
            this.radioButtonBig.TabStop = true;
            this.radioButtonBig.Text = "Grande";
            this.radioButtonBig.UseVisualStyleBackColor = true;
            // 
            // groupeBoxeSize
            // 
            this.groupeBoxeSize.Controls.Add(this.radioButtonBig);
            this.groupeBoxeSize.Controls.Add(this.radioButtonMid);
            this.groupeBoxeSize.Controls.Add(this.radioButtonSmall);
            this.groupeBoxeSize.Location = new System.Drawing.Point(26, 54);
            this.groupeBoxeSize.Name = "groupeBoxeSize";
            this.groupeBoxeSize.Size = new System.Drawing.Size(128, 189);
            this.groupeBoxeSize.TabIndex = 8;
            this.groupeBoxeSize.TabStop = false;
            this.groupeBoxeSize.Text = "Size";
            // 
            // groupeBoxIngredients
            // 
            this.groupeBoxIngredients.Controls.Add(this.checkBoxVegi);
            this.groupeBoxIngredients.Controls.Add(this.checkBoxMushrooms);
            this.groupeBoxIngredients.Controls.Add(this.checkBoxJam);
            this.groupeBoxIngredients.Location = new System.Drawing.Point(186, 54);
            this.groupeBoxIngredients.Name = "groupeBoxIngredients";
            this.groupeBoxIngredients.Size = new System.Drawing.Size(142, 189);
            this.groupeBoxIngredients.TabIndex = 9;
            this.groupeBoxIngredients.TabStop = false;
            this.groupeBoxIngredients.Text = "Ingredients";
            // 
            // PizzaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 384);
            this.Controls.Add(this.groupeBoxIngredients);
            this.Controls.Add(this.groupeBoxeSize);
            this.Controls.Add(this.AddPizza);
            this.Name = "PizzaForm";
            this.Text = "PizzaForm";
            this.groupeBoxeSize.ResumeLayout(false);
            this.groupeBoxeSize.PerformLayout();
            this.groupeBoxIngredients.ResumeLayout(false);
            this.groupeBoxIngredients.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AddPizza;
        private System.Windows.Forms.CheckBox checkBoxJam;
        private System.Windows.Forms.CheckBox checkBoxMushrooms;
        private System.Windows.Forms.CheckBox checkBoxVegi;
        private System.Windows.Forms.RadioButton radioButtonSmall;
        private System.Windows.Forms.RadioButton radioButtonMid;
        private System.Windows.Forms.RadioButton radioButtonBig;
        private System.Windows.Forms.GroupBox groupeBoxeSize;
        private System.Windows.Forms.GroupBox groupeBoxIngredients;
    }
}