﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using School;

namespace SchoolGUI
{
    public partial class StudentListForm : Form
    {
        public StudentListForm()
        {
            InitializeComponent();
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            StudentForm form = new StudentForm(null);
            if (form.ShowDialog() == DialogResult.OK)
            {
                lstStudents.Items.Add(form.Student);
            }
        }

        private void cmdRemove_Click(object sender, EventArgs e)
        {
            if (lstStudents.SelectedIndex != -1)
            {
                lstStudents.Items.Remove(lstStudents.SelectedItem);
            }
        }

        private void cmdModify_Click(object sender, EventArgs e)
        {
            if (lstStudents.SelectedIndex != -1)
            {
                Student student = (Student)lstStudents.SelectedItem;
                StudentForm form = new StudentForm(student);
                if (form.ShowDialog() == DialogResult.OK)
                {
                    // Version pas user friendly (modifie l'ordre des items)
                    //lstStudents.Items.Remove(lstStudents.SelectedItem);
                    //lstStudents.Items.Add(student);

                    // Version ultime!!!
                    lstStudents.Items[lstStudents.SelectedIndex] = student;
                }
            }
        }
    }
}
