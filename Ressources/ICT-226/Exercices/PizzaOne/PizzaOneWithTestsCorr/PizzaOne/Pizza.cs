﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaOne
{
    public class Pizza
    {
        private double basePrice;
        private List<Ingredient> ingredients;

        public Pizza(double basePrice)
        {
            this.basePrice = basePrice;
            ingredients = new List<Ingredient>();
        }

        public void AddIngredient(Ingredient ingredient)
        {
            ingredients.Add(ingredient);
        }

        public double Price
        {
            get
            {
                double totalPrice = basePrice;
                foreach (Ingredient ingredient in ingredients)
                {
                    totalPrice += ingredient.Price;
                }
                return totalPrice;
            }
        }

        public override string ToString()
        {
            string details = "Pizza avec\n";
            foreach (Ingredient ingredient in ingredients)
            {
                details += $"{ingredient}\n";
            }
            return details;
        }
    }
}
