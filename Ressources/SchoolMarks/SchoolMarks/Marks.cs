﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMarks
{
	public class Marks
	{
		private List<double> marks;

		public Marks()
		{
			marks = new List<double>();
		}

		public void Add(double mark)
		{
			marks.Add(mark);
		}


		public double Compare()
		{
			double min = 0.0;
			double max = 0.0;

			foreach (double currentMark in marks)
			{
				if (currentMark > max)
				{

					max = currentMark;
					if (currentMark > max)
					{
						max = currentMark;
					}
				
				}
				
			}

			return max;

		} 
		public double ComputeMean()
		{
			double sum = 0.0;
			
			foreach (double currentMark in marks)
			{
			
				sum += currentMark;
			}

			return sum / marks.Count;
		}
	}
}
