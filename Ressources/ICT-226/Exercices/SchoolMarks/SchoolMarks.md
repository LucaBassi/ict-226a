# SchoolMarks - Gestion des notes

Programme permettant de calculer une moyenne de notes.

## Elements marquants

1. Séparation du code métier (Business Logic) et de l'interface utilisateur (UI)
2. Encapsulation
3. Constructeur
4. Interpolation de chaines de caractères

## Travail à effectuer

Ajouter la fonctionnalité suivante:

Le programme affiche en plus de la moyenne, la note la meilleur ainsi que la pire.

ATTENTION: Respectez la séparation **BL** / **UI**
