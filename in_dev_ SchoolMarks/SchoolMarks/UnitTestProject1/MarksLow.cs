﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolMarks;

namespace UnitTestProject1
{
    [TestClass]
    public class MarksLow
    {
        [TestMethod]
        public void GetLowTest()
        {
            Marks marks = new Marks();

            marks.Add(5.5);
            marks.Add(4.0);
            marks.Add(5.5);
            marks.Add(5.5);
            double highExpected = 4.0;
            Assert.AreEqual(expected: highExpected, marks.Worst());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTestEmpty()
        {
            Marks marks = new Marks();


            double highExpected = 0;

            Assert.AreEqual(expected: highExpected, marks.Worst());


        }


    }
}

