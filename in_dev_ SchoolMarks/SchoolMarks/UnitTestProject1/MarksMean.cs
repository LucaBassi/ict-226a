﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolMarks;

namespace SchoolMarksTest
{
    [TestClass]
    public class MarksTest
    {
        [TestMethod]
        public void GetMeanTest()
        {
            Marks marks = new Marks();

            marks.Add(5.5);
            marks.Add(4.0);
            marks.Add(5.5);
            marks.Add(5.5);
            double MoyenneExpected =5.125 ;
            Assert.AreEqual(expected: MoyenneExpected, marks.ComputeMean());
        }



        [TestMethod]
        public void GetMeanTest1()
        {
            Marks marks = new Marks();

            marks.Add(5.5);
        
            double MoyenneExpected = 5.5;
            Assert.AreEqual(expected: MoyenneExpected, marks.ComputeMean());
        }



        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTestEqual()
        {
            Marks marks = new Marks();


            double highExpected = 0;

            Assert.AreEqual(expected: highExpected, marks.ComputeMean());


        }


    }


}
