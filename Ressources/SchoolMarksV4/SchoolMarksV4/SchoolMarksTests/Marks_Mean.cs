﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolMarks;

namespace SchoolMarksTests
{
    [TestClass]
    public class Marks_Mean
    {
        [TestMethod]
        public void Mean_With_3_Marks()
        {
            Marks marks = new Marks();

            marks.Add(6.0);
            marks.Add(3.0);
            marks.Add(4.0);

            double expectedMean = 4.333;
            double computedMean = marks.ComputeMean();

            Assert.AreEqual(expectedMean, computedMean, 0.01);
        }

        [TestMethod]
        public void Mean_With_1_Mark()
        {
            Marks marks = new Marks();

            marks.Add(3.0);

            double expectedMean = 3.0;
            double computedMean = marks.ComputeMean();

            Assert.AreEqual(expectedMean, computedMean, 0.01);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Mean_With_No_Mark()
        {
            Marks marks = new Marks();

            double computedMean = marks.ComputeMean();

            // Il n'y a pas d'Assert, car une exception a du être jetée
        }
    }
}
