﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PizzaOne;

namespace PizzaOneInterface
{
    public partial class OrderForm : Form
    {
        Order order = new Order();

        public OrderForm()
        {
            InitializeComponent();
        }

        private void cmdAddPizza_Click(object sender, EventArgs e)
        {
            PizzaCompositionForm form = new PizzaCompositionForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                order.AddPizza(form.Pizza);
            }    
        }

        private void cmdCheckout_Click(object sender, EventArgs e)
        {
            order.ClientName = txtClientName.Text;
            MessageBox.Show(order.Receipt);
        }
    }
}
