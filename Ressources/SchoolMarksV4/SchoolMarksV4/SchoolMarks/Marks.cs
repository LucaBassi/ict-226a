﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMarks
{
	public class Marks
	{
		private List<double> marks;
		private double best;
		private double worst;

		public Marks()
		{
			marks = new List<double>();
			best = 0.0;
			worst = 6.0;
		}

		public void Add(double mark)
		{
			marks.Add(mark);

			if (mark > best) best = mark;
			if (mark < worst) worst = mark;
		}

		public double ComputeMean()
		{
			EnsureMarksExists();

			double sum = 0.0;
			foreach (double currentMark in marks)
			{
				sum += currentMark;
			}

			return sum / marks.Count;
		}

		public double GetBest()
		{
			EnsureMarksExists();
			return best;
		}

		public double GetWorst()
		{
			EnsureMarksExists();
			return worst;
		}

		private void EnsureMarksExists()
		{
			if (marks.Count == 0)
			{
				throw new Exception("Not enough marks");
			}
		}
	}
}
