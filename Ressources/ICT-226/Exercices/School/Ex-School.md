---
title: Exercice - School
author: "[Pascal Hurni](mailto:phi@cpnv.ch)"
date:
- 19.05.2020
signature: PHI
banner: ICT 226a
colorlinks: true
fontsize: 12pt
listings-disable-line-numbers: true
numbersections: true
---

# School

Il s'agit de créer un nouveau projet en vue de gérer les étudiants d'une école comme la notre.

Vous procéderez en plusieurs étapes.


## Création de la partie _Business Logic_

Créez un nouveau projet de type **Class Library (.NET Framework)**, copie d'écran ci-dessous:

![Types de projets](ProjectTypes.png)

Vous devez respecter les spécifications ci-dessous mais en mettant en _anglais_ les différents éléments.
(Rappel: le code en français est autant pénible que s'il était en allemand).

Voici les spécifications:

### classe `Student`

Cette classe qui représente un élève dans l'école contiendra les **membres** suivants:

- **attribut** nom de famille
- **attribut** prénom
- **attribut** nom de classe (de l'école)
- **constructeur** spécialisé prenant en paramètre le nom et le prénom (valeurs initiales)
- **méthode** retournant le nom de famille
- **méthode** retournant le prénom
- **méthode** définissant la classe (de l'école).
- **méthode** retournant la classe (de l'école).

Voici les contraintes:

Le nom de famille est automatiquement convertit en majuscule.
Le prénom DOIT commencer par une majuscule.

Attention le nom de la classe doit respecter la nomenclature suivante:

  - commence par `SI-`
  - suivit de soit `C`, `MI` ou `CA`
  - suivit d'un chiffre entre `1` et `4`
  - le dernier caractère est forcément une lettre minuscule



## Ajout du projet de test

Pour prouver que le code métier a été correctement (selon les spécifications ci-avant) implémenté, vous ajouterez un projet de test
avec tous les tests unitaires nécessaires.

