﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMarks
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Marks marks = new Marks();

				Console.WriteLine("Bonjour, veuillez saisir vos notes, puis finir par une saisie vide");

				while (true)
				{
					string line = Console.ReadLine();
					if (line == "") break;

					double lineValue;
					if (double.TryParse(line, out lineValue))
					{
						marks.Add(lineValue);
					}
					else
					{
						Console.WriteLine("Valeur erronée, non prise en compte");
					}

				}

				Console.WriteLine($"Voici votre moyenne: {marks.ComputeMean()}");
				Console.WriteLine($"Votre meilleur note: {marks.GetBest()}, la pire: {marks.GetWorst()}");
			}
			catch (Exception exception)
			{
				Console.WriteLine($"Désolé, un problème est survenu: {exception.Message}");
			}
		}
	}
}
