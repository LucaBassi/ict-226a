﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMarks
{
	public class Marks
	{
		
		private List<double> marks;

		public Marks()
		{
			marks = new List<double>();

		}

		public void Add(double mark)
		{
		
			
			if (mark % 0.5 != 0)
			{
				throw new Exception("valeur doit etre arrondie au 0.5  ! ");
			//	Console.WriteLine("salut");
			//	throw new Exception($"calcul:{mark % 0.5}");
			}
			
			if (mark <= 1 || mark >= 6 )
			{
				throw new Exception("valeur trop haute ou trop basse ! ");
			}
		
	
			marks.Add(mark);
		}

		public double ComputeMean()
		{
			if (marks.Count == 0) 
			{
				throw new Exception("pas de marks dans le tableau");
			}
			double sum = 0.0;
			foreach (double currentMark in marks)
			{
				sum += currentMark;
			}

			return sum / marks.Count;
		}

		public double Worst()
		{

			if (marks.Count == 0)
			{
				throw new Exception("pas de marks dans le tableau");
			}

			double number = 6;
			foreach (double Note in marks)
			{
				if (Note < number)
				{
					number = Note;
				}

			}
			return number;
		}



		public double Biggest()
		{
			if (marks.Count == 0)
			{
				throw new Exception("pas de marks dans le tableau");
			}

			double number = 0;
			foreach( double Note in marks)
			{
				if (Note>number)
				{
					number = Note;
				}
				
			}
			return number;
		}


		


	}
}
