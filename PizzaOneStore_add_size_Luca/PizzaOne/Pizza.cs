﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaOne
{
    public class Pizza
    {
        private double basePrice;
        private List<Ingredient> ingredients;
        private Size size;


        public Pizza(double basePrice)
        {
            this.basePrice = basePrice;
            ingredients = new List<Ingredient>();
            
        }

        public void AddIngredient(Ingredient ingredient)
        {
            ingredients.Add(ingredient);
        }

        public void ChooseSize(Size size)
        {
            this.size = size;
        }





        public double Price
        {
            get
            {
                double totalPrice = basePrice;
                foreach (Ingredient ingredient in ingredients)
                {
                    
                    totalPrice += ingredient.Price;
                   
                }
                totalPrice += size.Price;

                return totalPrice;
            }
        }

        public override string ToString()
        {
            string details = $"Pizza de taille {size} avec: ";
            foreach (Ingredient ingredient in ingredients)
            {
                details += $"{ingredient}, ";
            }
            return details;
        }
    }
}
