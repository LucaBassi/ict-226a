﻿namespace ClientRegister
{
    partial class CommandForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdAddPizza = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmdAddPizza
            // 
            this.cmdAddPizza.Location = new System.Drawing.Point(114, 185);
            this.cmdAddPizza.Name = "cmdAddPizza";
            this.cmdAddPizza.Size = new System.Drawing.Size(104, 36);
            this.cmdAddPizza.TabIndex = 0;
            this.cmdAddPizza.Text = "Ajouter une Pizza";
            this.cmdAddPizza.UseVisualStyleBackColor = true;
            this.cmdAddPizza.Click += new System.EventHandler(this.cmdAddPizza_Click);
            // 
            // CommandForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 328);
            this.Controls.Add(this.cmdAddPizza);
            this.Name = "CommandForm";
            this.Text = "Home";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmdAddPizza;
    }
}

