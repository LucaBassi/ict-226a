﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PizzaOne;

namespace PizzaOneInterface
{
    public partial class PizzaCompositionForm : Form
    {
        Pizza pizza = new Pizza(10.0);

        public PizzaCompositionForm()
        {
            InitializeComponent();
        }

        public Pizza Pizza
        {
            get { return pizza; }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {




            if (chkHam.Checked)
            {
                pizza.AddIngredient(new Ingredient("Jambon", 2.5));
            }
            if (chkChorizo.Checked)
            {
                Ingredient ing;
                ing = new Ingredient("Chorizo", 1.5);
                pizza.AddIngredient(ing);
            }
            if (chkPeperoni.Checked)
            {
                pizza.AddIngredient(new Ingredient("Poivrons", 4.5));
            }






            string size = null;

            foreach (RadioButton radioButton in groupBoxSize.Controls)
            {
                if (radioButton.Checked == true)
                {

                    if (radioButtonSmall.Checked)
                    {
                        pizza.ChooseSize(new PizzaOne.Size(radioButton.Text,0.0));

                    }

                    if (radioButtonMiddle.Checked)
                    {
                        pizza.ChooseSize(new PizzaOne.Size(radioButton.Text,4.0));

                    }
                    
                    if (radioButtonBig.Checked)
                    {
                        pizza.ChooseSize(new PizzaOne.Size(radioButton.Text,8.0));

                    }
                    size = radioButton.Text;
                }
            }

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
