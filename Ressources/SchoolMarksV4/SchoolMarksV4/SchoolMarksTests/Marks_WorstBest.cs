﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolMarks;

namespace SchoolMarksTests
{
    [TestClass]
    public class Marks_WorstBest
    {
        [TestMethod]
        public void Best_With_3_Marks()
        {
            Marks marks = new Marks();

            marks.Add(5.5);
            marks.Add(3.0);
            marks.Add(4.0);

            double expected = 5.5;
            double computed = marks.GetBest();

            Assert.AreEqual(expected, computed);
        }

        [TestMethod]
        public void Best_With_1_Mark()
        {
            Marks marks = new Marks();

            marks.Add(2.0);

            double expected = 2.0;
            double computed = marks.GetBest();

            Assert.AreEqual(expected, computed);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Best_With_No_Mark()
        {
            Marks marks = new Marks();

            double computed = marks.GetBest();
        }

        [TestMethod]
        public void Worst_With_3_Marks()
        {
            Marks marks = new Marks();

            marks.Add(5.5);
            marks.Add(2.5);
            marks.Add(4.0);

            double expected = 2.5;
            double computed = marks.GetWorst();

            Assert.AreEqual(expected, computed);
        }

        [TestMethod]
        public void Worst_With_1_Mark()
        {
            Marks marks = new Marks();

            marks.Add(5.5);

            double expected = 5.5;
            double computed = marks.GetWorst();

            Assert.AreEqual(expected, computed);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Worst_With_No_Mark()
        {
            Marks marks = new Marks();

            double computed = marks.GetWorst();
        }
    }
}
