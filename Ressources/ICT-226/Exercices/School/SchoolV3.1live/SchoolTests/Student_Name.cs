﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using School;

namespace SchoolTests
{
    [TestClass]
    public class Student_Name
    {
        [TestMethod]
        public void Lastname_Transforms_To_Upper()
        {
            Student student = new Student("Pascal", "hurni");

            Assert.AreEqual("HURNI", student.Lastname);
        }

        [TestMethod]
        public void Firstname_Has_First_Capital()
        {
            Student student = new Student("Pascal", "hurni");

            Assert.AreEqual("Pascal", student.Firstname);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))] 
        public void Firstname_Has_No_First_Capital()
        {
            Student student = new Student("pascal", "hurni");
        }
    }
}
