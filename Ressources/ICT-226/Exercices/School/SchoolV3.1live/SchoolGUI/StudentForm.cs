﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using School;

namespace SchoolGUI
{
    public partial class StudentForm : Form
    {
        private Student student;

        public Student Student
        {
            get { return student; }
        }

        public StudentForm(Student student)
        {
            InitializeComponent();
            this.student = student;

            if (student != null)
            {
                txtFirstname.Text = student.Firstname;
                txtLastname.Text = student.Lastname;
                txtSchoolClass.Text = student.SchoolClass;
            }
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (student != null)
                {
                    // Modif

                    student.SchoolClass = txtSchoolClass.Text;
                }
                else
                {
                    student = new Student(txtFirstname.Text, txtLastname.Text);
                    student.SchoolClass = txtSchoolClass.Text;
                }

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Les données saisies sont invalides", "Erreur");
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
