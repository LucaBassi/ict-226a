﻿namespace PizzaOneInterface
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtClientName = new System.Windows.Forms.TextBox();
            this.cmdAddPizza = new System.Windows.Forms.Button();
            this.cmdCheckout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom du client";
            // 
            // txtClientName
            // 
            this.txtClientName.Location = new System.Drawing.Point(92, 88);
            this.txtClientName.Name = "txtClientName";
            this.txtClientName.Size = new System.Drawing.Size(347, 31);
            this.txtClientName.TabIndex = 1;
            // 
            // cmdAddPizza
            // 
            this.cmdAddPizza.Location = new System.Drawing.Point(92, 182);
            this.cmdAddPizza.Name = "cmdAddPizza";
            this.cmdAddPizza.Size = new System.Drawing.Size(347, 64);
            this.cmdAddPizza.TabIndex = 2;
            this.cmdAddPizza.Text = "Ajouter une pizza";
            this.cmdAddPizza.UseVisualStyleBackColor = true;
            this.cmdAddPizza.Click += new System.EventHandler(this.cmdAddPizza_Click);
            // 
            // cmdCheckout
            // 
            this.cmdCheckout.Location = new System.Drawing.Point(92, 388);
            this.cmdCheckout.Name = "cmdCheckout";
            this.cmdCheckout.Size = new System.Drawing.Size(347, 66);
            this.cmdCheckout.TabIndex = 3;
            this.cmdCheckout.Text = "Terminer la commande";
            this.cmdCheckout.UseVisualStyleBackColor = true;
            this.cmdCheckout.Click += new System.EventHandler(this.cmdCheckout_Click);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 530);
            this.Controls.Add(this.cmdCheckout);
            this.Controls.Add(this.cmdAddPizza);
            this.Controls.Add(this.txtClientName);
            this.Controls.Add(this.label1);
            this.Name = "OrderForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtClientName;
        private System.Windows.Forms.Button cmdAddPizza;
        private System.Windows.Forms.Button cmdCheckout;
    }
}

