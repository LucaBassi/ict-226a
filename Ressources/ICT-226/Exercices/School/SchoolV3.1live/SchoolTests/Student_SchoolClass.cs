﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using School;

namespace SchoolTests
{
    [TestClass]
    public class Student_SchoolClass
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Has_Wrong_Prefix()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "XI-C1a";
        }

        [TestMethod]
        public void Has_Correct_Prefix()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C1a";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Has_Wrong_Way()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-X1a";
        }

        [TestMethod]
        public void Has_Correct_Way()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C1a";
            student.SchoolClass = "SI-MI1a";
            student.SchoolClass = "SI-CA1a";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Has_Wrong_Year()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C5a";
        }

        [TestMethod]
        public void Has_Correct_Year()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C1a";
            student.SchoolClass = "SI-C2a";
            student.SchoolClass = "SI-C3a";
            student.SchoolClass = "SI-C4a";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Has_Wrong_Sibling()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C1Z";
        }

        [TestMethod]
        public void Has_Correct_Sibling()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C1a";
            student.SchoolClass = "SI-C2g";
            student.SchoolClass = "SI-C3p";
            student.SchoolClass = "SI-C4z";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Wrong_Complete_Names()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-MIlanais3p";
            student.SchoolClass = "SI-CACOPHONIE1s";
        }

        [TestMethod]
        public void Correct_Complete_Names()
        {
            Student student = new Student("Pascal", "Hurni");
            student.SchoolClass = "SI-C1a";
            student.SchoolClass = "SI-MI4a";
        }
    }
}
