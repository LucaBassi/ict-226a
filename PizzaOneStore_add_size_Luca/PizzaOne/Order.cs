﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaOne
{
    public class Order
    {
        private string clientName;
        private List<Pizza> pizzas = new List<Pizza>();

        public void AddPizza(Pizza pizza)
        {
            pizzas.Add(pizza);
        }

        public string ClientName
        {
            set { clientName = value; }
        }

        public double TotalPrice
        {
            get
            {
                double total = 0.0;
                foreach (Pizza pizza in pizzas)
                {
                    total += pizza.Price;
                }
                return total;
            }
        }

        public string Receipt
        {
            get
            {
                string receipt = $"{clientName}\n";
                foreach (Pizza pizza in pizzas)
                {
                    receipt += $"  {pizza.Price} {pizza}\n";
                }
                receipt += $"Total: {TotalPrice}";
                return receipt;
            }
        }

    }
}
