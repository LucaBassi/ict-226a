﻿namespace PizzaOneInterface
{
    partial class PizzaCompositionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkPeperoni = new System.Windows.Forms.CheckBox();
            this.chkChorizo = new System.Windows.Forms.CheckBox();
            this.chkHam = new System.Windows.Forms.CheckBox();
            this.cmdOk = new System.Windows.Forms.Button();
            this.groupBoxSize = new System.Windows.Forms.GroupBox();
            this.radioButtonSmall = new System.Windows.Forms.RadioButton();
            this.radioButtonMiddle = new System.Windows.Forms.RadioButton();
            this.radioButtonBig = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBoxSize.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkPeperoni);
            this.groupBox1.Controls.Add(this.chkChorizo);
            this.groupBox1.Controls.Add(this.chkHam);
            this.groupBox1.Location = new System.Drawing.Point(242, 28);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(226, 155);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ingrédients";
            // 
            // chkPeperoni
            // 
            this.chkPeperoni.AutoSize = true;
            this.chkPeperoni.Location = new System.Drawing.Point(18, 101);
            this.chkPeperoni.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkPeperoni.Name = "chkPeperoni";
            this.chkPeperoni.Size = new System.Drawing.Size(67, 17);
            this.chkPeperoni.TabIndex = 2;
            this.chkPeperoni.Text = "Poivrons";
            this.chkPeperoni.UseVisualStyleBackColor = true;
            // 
            // chkChorizo
            // 
            this.chkChorizo.AutoSize = true;
            this.chkChorizo.Location = new System.Drawing.Point(18, 64);
            this.chkChorizo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkChorizo.Name = "chkChorizo";
            this.chkChorizo.Size = new System.Drawing.Size(61, 17);
            this.chkChorizo.TabIndex = 1;
            this.chkChorizo.Text = "Chorizo";
            this.chkChorizo.UseVisualStyleBackColor = true;
            // 
            // chkHam
            // 
            this.chkHam.AutoSize = true;
            this.chkHam.Location = new System.Drawing.Point(18, 28);
            this.chkHam.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkHam.Name = "chkHam";
            this.chkHam.Size = new System.Drawing.Size(63, 17);
            this.chkHam.TabIndex = 0;
            this.chkHam.Text = "Jambon";
            this.chkHam.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.Location = new System.Drawing.Point(104, 207);
            this.cmdOk.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(276, 37);
            this.cmdOk.TabIndex = 1;
            this.cmdOk.Text = "Valider";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // groupBoxSize
            // 
            this.groupBoxSize.Controls.Add(this.radioButtonBig);
            this.groupBoxSize.Controls.Add(this.radioButtonMiddle);
            this.groupBoxSize.Controls.Add(this.radioButtonSmall);
            this.groupBoxSize.Location = new System.Drawing.Point(12, 28);
            this.groupBoxSize.Name = "groupBoxSize";
            this.groupBoxSize.Size = new System.Drawing.Size(212, 154);
            this.groupBoxSize.TabIndex = 2;
            this.groupBoxSize.TabStop = false;
            this.groupBoxSize.Text = "Taille";
            // 
            // radioButtonSmall
            // 
            this.radioButtonSmall.AutoSize = true;
            this.radioButtonSmall.Location = new System.Drawing.Point(24, 28);
            this.radioButtonSmall.Name = "radioButtonSmall";
            this.radioButtonSmall.Size = new System.Drawing.Size(52, 17);
            this.radioButtonSmall.TabIndex = 0;
            this.radioButtonSmall.TabStop = true;
            this.radioButtonSmall.Text = "Petite";
            this.radioButtonSmall.UseVisualStyleBackColor = true;
            // 
            // radioButtonMiddle
            // 
            this.radioButtonMiddle.AutoSize = true;
            this.radioButtonMiddle.Location = new System.Drawing.Point(24, 64);
            this.radioButtonMiddle.Name = "radioButtonMiddle";
            this.radioButtonMiddle.Size = new System.Drawing.Size(56, 17);
            this.radioButtonMiddle.TabIndex = 1;
            this.radioButtonMiddle.TabStop = true;
            this.radioButtonMiddle.Text = "Middle";
            this.radioButtonMiddle.UseVisualStyleBackColor = true;
            // 
            // radioButtonBig
            // 
            this.radioButtonBig.AutoSize = true;
            this.radioButtonBig.Location = new System.Drawing.Point(24, 101);
            this.radioButtonBig.Name = "radioButtonBig";
            this.radioButtonBig.Size = new System.Drawing.Size(60, 17);
            this.radioButtonBig.TabIndex = 2;
            this.radioButtonBig.TabStop = true;
            this.radioButtonBig.Text = "Grande";
            this.radioButtonBig.UseVisualStyleBackColor = true;
            // 
            // PizzaCompositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 274);
            this.Controls.Add(this.groupBoxSize);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "PizzaCompositionForm";
            this.Text = "PizzaCompositionForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxSize.ResumeLayout(false);
            this.groupBoxSize.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkPeperoni;
        private System.Windows.Forms.CheckBox chkChorizo;
        private System.Windows.Forms.CheckBox chkHam;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.GroupBox groupBoxSize;
        private System.Windows.Forms.RadioButton radioButtonBig;
        private System.Windows.Forms.RadioButton radioButtonMiddle;
        private System.Windows.Forms.RadioButton radioButtonSmall;
    }
}